#pop() and popitem()
email={'haresh':'hareshsolnaki8@gmail.com',
	'abc':'abc@gmail.com',
	'def':'def@yahoo.com',
	'ghi':'ghi@gmail.com',
	'jkl':'jkl@gmail.com',
	'mno':'mno@gmail.com',
	'pqr':'pqr@gmail.com',
	'stu':'stu@gmail.com',
	'vwxyz':'vwxyz@gmail.com'}

print email
print

email.pop('abc')
email.pop('haresh')

print email
print

email.popitem()
print email
