# creating two list and perform append() and extend()

list1=[1,2,3,4,5,6]
list2=[1,2,3,4,5,6]

list1.append([111,222,333])   # this append list into list.
list1.extend([555,888,666])   # this adds each as single element

print list1
print list2
